{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
module Data where

import           Google.Geocode

data1 :: GeocodeResponse
data1 = GeocodeResponse
  { results = [ GeocodeResult
                  { accessPoints      = []
                  , addressComponents = []
                  , formattedAddress  = "abc"
                  , geometry          = Geometry
                                          (LocationShort 21 22)
                                          Rooftop
                                          (Viewport (LocationShort 33 34)
                                                    (LocationShort 35 36)
                                          )
                  , placeId           = "abc"
                  , segmentPosition   = Nothing
                  , plusCode          = PlusCode "compoundCode" "globalCode"
                  , types             = []
                  }
              ]
  , status  = OK
  }
