{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeOperators #-}
-- |
-- Module:      Google.Geocode
-- Copyright:   (c) 2020 Torsten Kemps-Benedix
--              (c) 2020 ConZone GmbH
-- License:     BSD3
-- Maintainer:  Torsten Kemps-Benedix <tkx68@icloud.com>
-- Stability:   experimental
-- Portability: portable
--
-- Types and functions for accessing the Google Maps Geocoding API.
module Google.Geocode
  (
    -- * How to use this library
    -- $use
    -- * Functions
    geocode
  , geocode'
  -- * Data Types
  , GeocodeResponse(..)
  , LocationShort(..)
  , LocationType(..)
  , Viewport(..)
  , Geometry(..)
  , AddressComponent(..)
  , PlusCode(..)
  , AccessPointType(..)
  , AccessPoint(..)
  , GeocodeResult(..)
  , StatusCode(..)
  , Location(..)
  , AddressType(..)
  )
where

import           Data.Aeson
import           Data.Aeson.TH
import           Data.Char
import           Data.Proxy
import           Data.Text                      ( Text )
import           GHC.Generics
import           Network.HTTP.Client.TLS        ( newTlsManagerWith
                                                , tlsManagerSettings
                                                )
import           Servant.API
import           Servant.Client
import           Data.Aeson.TH.Modifiers

data AddressType =
    StreetAddress -- ^ indicates a precise street address.
    | Route -- ^ indicates a named route (such as "US 101").
    | Intersection  -- ^ indicates a major intersection, usually of two major roads.
    | Political  -- ^ indicates a political entity. Usually, this type indicates a polygon of some civil administration.
    | Country  -- ^ indicates the national political entity, and is typically the highest order type returned by the Geocoder.
    | AdministrativeAreaLevel1  -- ^ indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels. In most cases, administrative_area_level_1 short names will closely match ISO 3166-2 subdivisions and other widely circulated lists; however this is not guaranteed as our geocode results are based on a variety of signals and location data.
    | AdministrativeAreaLevel2  -- ^ indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
    | AdministrativeAreaLevel3  -- ^ indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    | AdministrativeAreaLevel4  -- ^ indicates a fourth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    | AdministrativeAreaLevel5  -- ^ indicates a fifth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
    | ColloquialArea  -- ^ indicates a commonly-used alternative name for the entity.
    | Locality  -- ^ indicates an incorporated city or town political entity.
    | Sublocality  -- ^ indicates a first-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | SublocalityLevel1  -- ^ indicates a first-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | SublocalityLevel2  -- ^ indicates a second-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | SublocalityLevel3  -- ^ indicates a third-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | SublocalityLevel4  -- ^ indicates a fourth-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | SublocalityLevel5  -- ^ indicates a fifth-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
    | Neighborhood  -- ^ indicates a named neighborhood
    | Premise  -- ^ indicates a named location, usually a building or collection of buildings with a common name
    | Subpremise  -- ^ indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
    | PlusCode1  -- ^ indicates an encoded location reference, derived from latitude and longitude. Plus codes can be used as a replacement for street addresses in places where they do not exist (where buildings are not numbered or streets are not named). See https://plus.codes for details.
    | PostalCode  -- ^ indicates a postal code as used to address postal mail within the country.
    | NaturalFeature  -- ^ indicates a prominent natural feature.
    | Airport  -- ^ indicates an airport.
    | Park  -- ^ indicates a named park.
    | PointOfInterest  -- ^ indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category, such as "Empire State Building" or "Eiffel Tower".
    | Floor  -- ^ indicates the floor of a building address.
    | Establishment  -- ^ typically indicates a place that has not yet been categorized.
    | Parking  -- ^ indicates a parking lot or parking structure.
    | PostBox  -- ^ indicates a specific postal box.
    | PostalTown  -- ^ indicates a grouping of geographic areas, such as locality and sublocality, used for mailing addresses in some countries.
    | Room  -- ^ indicates the room of a building address.
    | StreetNumber  -- ^ indicates the precise street number.
    | BusStation  -- ^ indicates the location of a bus stop.
    | TrainStation   -- ^ indicates the location of a train stop.
    | TransitStation  -- ^ indicates the location of a public transit stop.
    deriving (Show, Generic)
$(deriveJSON defaultOptions{constructorTagModifier = repl [("plus_code_1", "plus_code")] . hs2c} ''AddressType)

data Location  = Location {latitude :: Double, longitude :: Double} deriving (Show, Generic)
$(deriveJSON defaultOptions ''Location)

data LocationShort  = LocationShort {lat :: Double, lng :: Double} deriving (Show, Generic)
$(deriveJSON defaultOptions ''LocationShort)

data LocationType = Rooftop | RangeInterpolated | GeometricCenter | Approximate deriving (Show, Generic)
$(deriveJSON defaultOptions{constructorTagModifier = map toUpper . hs2c} ''LocationType)

data Viewport = Viewport {
    northeast :: LocationShort,
    southwest :: LocationShort
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''Viewport)

data Geometry = Geometry {
    location :: LocationShort,
    locationType :: LocationType,
    viewport :: Viewport
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''Geometry)

data AddressComponent = AddressComponent {
    longName :: Text,
    shortName :: Text,
    types :: [AddressType]
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''AddressComponent)

data PlusCode = PlusCode {
    compoundCode :: Text,
    globalCode :: Text
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''PlusCode)

data AccessPointType = TYPE_SEGMENT deriving (Show, Generic)
$(deriveJSON defaultOptions{allNullaryToStringTag=False} ''AccessPointType)

data AccessPoint = AccessPoint {
    accessPointType :: Text,
    location :: Location,
    locationOnSegment :: Location,
    placeId :: Text,
    segmentPosition :: Maybe Double,
    unsuitableTravelModes :: [Text] -- ^ ? Data type?
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''AccessPoint)

data   GeocodeResult =   GeocodeResult {
    accessPoints :: [AccessPoint],
    addressComponents :: [AddressComponent],
    formattedAddress :: Text,
    geometry :: Geometry,
    placeId :: Text,
    segmentPosition :: Maybe Double,
    plusCode :: PlusCode,
    types :: [AddressType]
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} ''  GeocodeResult)

data StatusCode = OK | ZeroResults | OverDailyLimit | OverQueryLimit | RequestDenied | InvalidRequest | UnknownError
    deriving (Show, Generic)
$(deriveJSON defaultOptions{constructorTagModifier = repl [("O_K", "OK")] . map toUpper . hs2c} ''StatusCode)

data  GeocodeResponse =  GeocodeResponse {
  results :: [  GeocodeResult],
  status :: StatusCode
} deriving (Show, Generic)
$(deriveJSON defaultOptions{fieldLabelModifier = hs2c} '' GeocodeResponse)

type GeocodeApi
  = "maps" :> "api" :> "geocode" :> "json" :> QueryParam' '[Required] "address" Text :> QueryParam' '[Required] "key" Text :> Get '[JSON] [ GeocodeResponse]

geocodeApi :: Proxy GeocodeApi
geocodeApi = Proxy

geocode
  :: Text -- ^ Address, e.g. Rotwildschneise 24, Hamburg, Germany
  -> Text -- ^ Your Google Maps API access key
  -> ClientM [GeocodeResponse]
geocode = client geocodeApi

geocode'
  :: Text -- ^ Address, e.g. Rotwildschneise 24, Hamburg, Germany
  -> Text -- ^ Your Google Maps API access key
  -> IO (Either ClientError [GeocodeResponse])
geocode' adr accessKey = do
    -- https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=AIzaSyDpqW-xTqMY2D_hHehjBbykv3SorZJvsEc
  manager' <- newTlsManagerWith tlsManagerSettings
  runClientM
    (geocode adr accessKey)
    (mkClientEnv manager' (BaseUrl Https "maps.googleapis.com" 443 ""))

-- $use
-- "Geocoding is the process of converting addresses (like "1600 Amphitheatre Parkway, 
-- Mountain View, CA") into geographic coordinates (like latitude 37.423021 and 
-- longitude -122.083739), which you can use to place markers on a map, or position 
-- the map." (Google at <https://developers.google.com/maps/documentation/geocoding/intro>)
--
-- Example:
-- > :set -XOverloadedStrings
-- > geocode' "Rotwildschneise 24, Hamburg, Germany" "YOUR_ACCESS_KEY"

